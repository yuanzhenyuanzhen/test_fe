import axios from 'axios'

var instance = axios.create({
    headers:{
        'Content-Type':'application/json'
    },
    // baseURL:'http://stuq.ceshiren.com:8089/',
    baseURL:'http://127.0.0.1:8001/'
})

export default instance