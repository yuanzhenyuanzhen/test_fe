import axios from './http'

const testcase={
    getTestcase(){
        return axios({
            method: 'get',
            url: '/testcase',
          })
    },

    deleteTestcase(params){
        return axios({
            method:'delete',
            url:'/testcase',
            //传URL参数使用params
            params:{id:params}
        })
    },

    updateTestcase(params){
        return axios({
            method:'put',
            url:'/testcase',
            //传请求体使用data
            data:params
        })

    },

    addTestcase(params){
        return axios({
            method:'post',
            url:'/testcase',
            //传请求体使用data
            data:params
        })

    },

}

export default testcase