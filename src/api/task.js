import axios from './http'

const task={
    getTask(){
        return axios({
            method: 'get',
            url: '/task',
          })
    },

    addTask(params){
        return axios({
            method:'post',
            url:'/task',
            //传请求体使用data
            data:params
        })

    },

}

export default task