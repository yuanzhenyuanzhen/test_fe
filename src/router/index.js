import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../views/Layout'
import Testcase from '../views/Testcase'
import Task from '../views/Task'

Vue.use(VueRouter)

const routes = [
  {
    // 指定路由
    path: '/',
    // 指定渲染哪个组件
    // component: Home
    // 指定重定向页面
    redirect: "/layout"
  },
  {
    path: "/layout",
    component: Layout,
    children:[
      {
        path: "testcase",
        name: "testcase",
        component: Testcase
      },
      {
        path: "task",
        name: "task",
        component: Task
      }
    ]
  },
   
]

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  // base: process.env.BASE_URL,
  routes
})

export default router
